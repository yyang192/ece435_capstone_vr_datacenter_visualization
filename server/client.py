#!/usr/bin/env python

import socket

# TCP_IP = '127.0.0.1'
# TCP_PORT = 5006
BUFFER_SIZE = 2048
UDP_IP = "127.0.0.1"
UDP_PORT = 5005
# MESSAGE = "Hello, World!"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# sock.connect((TCP_IP, TCP_PORT))

f = open('index_instruction.txt', 'r')
file_contents = ''
file_contents += f.read()
print(file_contents)
f.close()

while 1:
	# print('Enter index:')
	text = ''
	index = input()
	while not index:
		index = input(file_contents)
	sock.sendto(index.encode('utf-8'), (UDP_IP, UDP_PORT))
	msg, addr = sock.recvfrom(BUFFER_SIZE)
	data = msg.decode()
	text += data

	while text.find('END') == -1:
		msg, addr = sock.recvfrom(BUFFER_SIZE)
		data = msg.decode()
		text += data
	print(text[:-3])
	if text[:-3] == 'close':	break
# sock.close()
