import json
import requests
from requests.auth import HTTPBasicAuth

#GNS3 Information

GNS3_SERVER_ADDR = '127.0.0.1'
GNS3_SERVER_PORT = '3080'
GNS3_USERNAME = 'admin'
GNS3_PASSWORD = 'cs436'


#URL mappings
#Copied and modified from https://gns3-server.readthedocs.io/en/latest/endpoints.html on December 17, 2018
GNS3_URLS = {
    #Appliance
        'appliances': 'v2/appliances',
        'appliance_templates': 'v2/appliances/templates',
        'initialize_node_from_template': 'v2/projects/{project_id}/appliances/{appliance_id}',
    #TODO 'implement' later
    #Compute
    #    /v2/computes
    #    /v2/computes/endpoint/{compute_id}/{emulator}/{action:.+}
    #    /v2/computes/{compute_id}
    #    /v2/computes/{compute_id}/auto_idlepc
    #    /v2/computes/{compute_id}/{emulator}/{action:.+}
    #    /v2/computes/{compute_id}/{emulator}/images
    #Drawing
        'drawings': 'v2/projects/{project_id}/drawings',
        'drawing_instance': 'v2/projects/{project_id}/drawings/{drawing_id}',
    #Gns3 vm
        'vm_settings': 'v2/gns3vm',
        'vm_engines_supported': 'v2/gns3vm/engines',
        'engine_vms_supported': '/v2/gns3vm/engines/{engine}/vms',
    #Link
        'links': 'v2/projects/{project_id}/links',
        'link_instance': 'v2/projects/{project_id}/links/{link_id}',
        'link_filters': 'v2/projects/{project_id}/links/{link_id}/available_filters',
        'link_pcap': 'v2/projects/{project_id}/links/{link_id}/pcap',
        'link_start_capture': 'v2/projects/{project_id}/links/{link_id}/start_capture',
        'link_stop_capture': 'v2/projects/{project_id}/links/{link_id}/stop_capture',
    #Node
        'nodes': 'v2/projects/{project_id}/nodes',
        'node_instance': 'v2/projects/{project_id}/nodes/{node_id}',
        'duplicate_node': 'v2/projects/{project_id}/nodes/{node_id}/duplicate',
        'compute_node_idlepc': 'v2/projects/{project_id}/nodes/{node_id}/dynamips/auto_idlepc',
        'compute_node_potential_idlepcs': 'v2/projects/{project_id}/nodes/{node_id}/dynamips/idlepc_proposals',
        #'node_files': 'v2/projects/{project_id}/nodes/{node_id}/files/{path:.+}',
        'node_links': 'v2/projects/{project_id}/nodes/{node_id}/links',
        'reload_node': 'v2/projects/{project_id}/nodes/{node_id}/reload',
        'start_node': 'v2/projects/{project_id}/nodes/{node_id}/start',
        'stop_node': 'v2/projects/{project_id}/nodes/{node_id}/stop',
        'suspend_node': 'v2/projects/{project_id}/nodes/{node_id}/suspend',
        'reload_all_nodes': 'v2/projects/{project_id}/nodes/reload',
        'start_all_nodes': 'v2/projects/{project_id}/nodes/start',
        'stop_all_nodes': 'v2/projects/{project_id}/nodes/stop',
        'suspend__all_nodes': 'v2/projects/{project_id}/nodes/suspend',
    #Project
        'projects': 'v2/projects',
        'load_project': 'v2/projects/load',
        'project_instance': 'v2/projects/{project_id}',
        'close_project': 'v2/projects/{project_id}/close',
        'duplicate_project': '/v2/projects/{project_id}/duplicate',
        'export_project': 'v2/projects/{project_id}/export',
        #'access_project_file': 'v2/projects/{project_id}/files/{path:.+}',
        'import_project': 'v2/projects/{project_id}/import',
        'project_notifications': 'v2/projects/{project_id}/notifications',
        'project_notifications_websocket': 'v2/projects/{project_id}/notifications/ws',
        'open_project': 'v2/projects/{project_id}/open',
    #Server
        'debug': 'v2/debug',
        'settings' : 'v2/settings', 
        'shutdown': 'v2/shutdown', 
        'version': 'v2/version',
    #Snapshot
        'all_snapshots': 'v2/projects/{project_id}/snapshots',
        'snapshot_info': 'v2/projects/{project_id}/snapshots/{snapshot_id}',
        'restore_snapshot': '/v2/projects/{project_id}/snapshots/{snapshot_id}/restore'
}
#TODO implement later?
#Symbol
#/v2/symbols
#/v2/symbols/{symbol_id:.+}/raw

#Generate GNS3 address for requests
GNS3_ADDR = 'http://{0}:{1}'.format(GNS3_SERVER_ADDR, GNS3_SERVER_PORT)
GNS3_AUTH = HTTPBasicAuth(GNS3_USERNAME, GNS3_PASSWORD)

################################################################################
##########Appliance
################################################################################

def get_appliances_list():
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['appliances']), auth=GNS3_AUTH)
    return r.json()

#TODO finish the rest

################################################################################
##########Drawing
################################################################################

def get_drawings_list(project_id):
    drawings_url = GNS3_URLS['drawings'].format(project_id=project_id)
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, drawings_url), auth=GNS3_AUTH)
    return r.json()

def get_drawing(project_id, drawing_id):
    drawing_url = GNS3_URLS['drawing_instance'].format(project_id=project_id, drawing_id=drawing_id)
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, drawing_url), auth=GNS3_AUTH)
    return r.json()

def update_drawing(project_id, drawing_id):
    drawing_url = GNS3_URLS['drawing_instance'].format(project_id=project_id, drawing_id=drawing_id)
    r = requests.put('{0}/{1}'.format(GNS3_ADDR, drawing_url), auth=GNS3_AUTH)
    return r.json()

def delete_drawing(project_id, drawing_id):
    drawing_url = GNS3_URLS['drawing_instance'].format(project_id=project_id, drawing_id=drawing_id)
    r = requests.delete('{0}/{1}'.format(GNS3_ADDR, drawing_url), auth=GNS3_AUTH)
    #TODO what do we return?
    return r

#TODO next categories

################################################################################
#########Server
################################################################################

def dump_debug_info():
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['debug']), auth=GNS3_AUTH)
    #TODO what do we return?
    return r

def get_settings():
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['settings']), auth=GNS3_AUTH)
    return r.json()

def shutdown_server():
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['shutdown']), auth=GNS3_AUTH)
    #TODO what do we return?
    return r

def get_version():
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['version']), auth=GNS3_AUTH)
    return r.json()

def compare_version(version_to_compare):
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, GNS3_URLS['version']), auth=GNS3_AUTH,
        json={'version': str(version_to_compare)})
    return r


################################################################################
##########Implementation
################################################################################
def nodes_information(project_id):
    url = GNS3_URLS['nodes'].format(project_id=project_id)
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r.json()

def links_information(project_id):
    url = GNS3_URLS['links'].format(project_id=project_id)
    r = requests.get('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r.json()

def node_create(project_id, name, para):
    url = GNS3_URLS['nodes'].format(project_id=project_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH, data=para)
    return r

def node_delete(project_id, node_id):
    url = GNS3_URLS['node_instance'].format(project_id=project_id, node_id=node_id)
    r = requests.delete('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def link_create(project_id, node_id_1, node_id_2, adapter_1, adapter_2, port_1, port_2):
    para = '{"nodes": [{"adapter_number": '+str(adapter_1)+', "label": {"text": "Text", "x": 0, "y": 0}, "node_id": "'+str(node_id_1)+'", "port_number": '+str(port_1)+'}, {"adapter_number": '+str(adapter_2)+', "node_id": "'+str(node_id_2)+'", "port_number": '+str(port_2)+'}]}'
    url = GNS3_URLS['links'].format(project_id=project_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH, data=para)
    return r

def link_delete(project_id, link_id):
    url = GNS3_URLS['link_instance'].format(project_id=project_id, link_id=link_id)
    r = requests.delete('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_start_all(project_id):
    url = GNS3_URLS['start_all_nodes'].format(project_id=project_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_stop_all(project_id):
    url = GNS3_URLS['stop_all_nodes'].format(project_id=project_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_suspend_all(project_id):
    url = GNS3_URLS['suspend__all_nodes'].format(project_id=project_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_start(project_id, node_id):
    url = GNS3_URLS['start_node'].format(project_id=project_id, node_id=node_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_stop(project_id, node_id):
    url = GNS3_URLS['stop_node'].format(project_id=project_id, node_id=node_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r

def node_suspend(project_id, node_id):
    url = GNS3_URLS['suspend_node'].format(project_id=project_id, node_id=node_id)
    r = requests.post('{0}/{1}'.format(GNS3_ADDR, url), auth=GNS3_AUTH)
    return r



#Testing
# node_create(project_id, 10 ,0)
# print(get_version())
# get_settings()
# print(shutdown_server())
# print(compare_version('2.1.12dev1'))
# print(get_drawings_list('a'))