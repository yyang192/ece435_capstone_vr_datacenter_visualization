#!/usr/bin/env python

import socket
import gns3
import gns3_helper as gn
import time


# TCP_IP = '127.0.0.1'
# TCP_PORT = 5006
BUFFER_SIZE = 2048  # Normally 1024, but we want fast response
project_id ='83b40958-ede6-4dec-9cff-e11bdf38bbcc'
UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.bind((TCP_IP, TCP_PORT))
# s.listen(1)

# conn, addr = s.accept()
# print('Connection address:', addr)

############update information#########
nodes_info = gn.update_nodes_info()
links_info = gn.update_links_info()
tn = None

while 1:

  msg, addr = sock.recvfrom(BUFFER_SIZE)
  index = msg.decode('utf-8')

  if index == '00':
    data = str(gns3.get_version())+str('END')

  elif index == '01':
    nodes_info = gn.update_nodes_info()
    print(nodes_info)
    data = []
    for node in nodes_info:
      temp = []
      temp.append(node.get('name'))
      temp.append(node.get('console'))
      temp.append(list(node['ports'].keys()))
      data.append(temp)
    data = str(data)+str('END')

  elif index == '02':
    links_info = gn.update_links_info()
    data = str(links_info)+str('END')

  elif index[0:2] == '03':
    nodes_info = gn.update_nodes_info()
    node_type = int(index[2:4])
    name = len(nodes_info)+1
    data = str(gn.create_node(project_id, name, node_type))+str('END')

  elif index[:2] == '04':
    node_idx = int(index[2:4])
    nodes_info = gn.update_nodes_info()
    if node_idx>=0 and node_idx<len(nodes_info):
      node_id = nodes_info[node_idx].get('node_id')
      data = str(gns3.node_delete(project_id, node_id))+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[:2] == '05':
    nodes_info = gn.update_nodes_info()
    node_idx_1 = int(index[2:4])
    node_idx_2 = int(index[4:6])
    port_name_1 = index[6:10]
    port_name_2 = index[10:14]
    if node_idx_1>=0 and node_idx_1<len(nodes_info) and node_idx_2>=0 and node_idx_2<len(nodes_info) and node_idx_1!=node_idx_2:
      node_1 = nodes_info[node_idx_1]
      node_2 = nodes_info[node_idx_2]
      if port_name_1 in list(node_1.get('ports').keys()) and port_name_2 in list(node_2.get('ports').keys()):
        port_1 = node_1.get('ports').get(port_name_1)
        port_2 = node_2.get('ports').get(port_name_2)
        data = gns3.link_create(project_id, node_1.get('node_id'), node_2.get('node_id'), port_1.get('adapter_number'), port_2.get('adapter_number'), port_1.get('port_number'), port_2.get('port_number'))
        data = str(data)+str('END')
      else:
        data = str('incorrect port')+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[:2] == '06':
    links_info = gn.update_links_info()
    link_idx = int(index[2:4])
    if link_idx>=0 and link_idx<len(links_info):
      data = str(gns3.link_delete(project_id, links_info[link_idx].get('link_id')))+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index == '07':
    data = str(gns3.node_start_all(project_id))+str('END')

  elif index == '08':
    data = str(gns3.node_stop_all(project_id))+str('END')

  elif index == '09':
    data = str(gns3.node_suspend_all(project_id))+str('END')

  elif index[:2] == '10':
    node_idx = int(index[2:4])
    if node_idx>=0 and node_idx<len(nodes_info):
      node_id = nodes_info[node_idx].get('node_id')
      data = str(gns3.node_start(project_id, node_id))+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[:2] == '11':
    node_idx = int(index[2:4])
    if node_idx>=0 and node_idx<len(nodes_info):
      node_id = nodes_info[node_idx].get('node_id')
      data = str(gns3.node_stop(project_id, node_id))+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[:2] == '12':
    node_idx = int(index[2:4])
    if node_idx>=0 and node_idx<len(nodes_info):
      node_id = nodes_info[node_idx].get('node_id')
      data = str(gns3.node_suspend(project_id, node_id))+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[0:2] == '13':
    node_idx = int(index[2:4])
    if node_idx >= 0 and node_idx < len(nodes_info):
      node = nodes_info[node_idx]
      tn = gn.connect_console(node.get('console'), node.get('name'))
      data = str('connected')+str('END')
    else:
      data = str('incorrect index')+str('END')

  elif index[0:2] == '14':
    if tn:
      node_idx = int(index[2:4])
      cmd = index[4:]
      node = nodes_info[node_idx]
      data = str(gn.console_command(tn, node.get('name'), cmd))+str('END')
    else: 
      data = str('no connection')+str('END')

  elif index[0:2] == '15':
    if tn:
      gn.close_console(tn)
      tn = None
      data = str('closed')+str('END')
    else:
      data = str('no connection')+str('END')

  elif index == '-1':
    data = str('close')+str('END')

  else:
    data = str('incorrect msg')+str('END')
  sock.sendto(data.encode('utf-8'), addr)  # echo

