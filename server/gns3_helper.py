import gns3
import getpass
import sys
import telnetlib
import gc
import ast

project_id ='83b40958-ede6-4dec-9cff-e11bdf38bbcc'
HOST = "127.0.0.1"
dym_path = '/Users/mac/GNS3/projects/test/project-files/dynamips/'

def ports_info(node):
  ports_info = {}
  for port in node.get('ports'):
    port_info = {}
    port_info['adapter_number'] = port.get('adapter_number')
    port_info['port_number'] = port.get('port_number')
    ports_info[port.get('short_name')] = port_info
  # print(ports_info)
  return ports_info

def update_nodes_info():
  nodes_info = []
  info = gns3.nodes_information(project_id)
  data = ''
  for node in info:
    node_info = {}
    node_info['name'] = node.get('name')
    node_info['node_id'] = node.get('node_id')
    node_info['console'] = node.get('console')
    node_info['name'] = node.get('name')
    node_info['ports'] = ports_info(node)
    # print(node_info['ports'])
    if node.get('properties').get('platform'):
      node_info['platform'] = node.get('properties').get('platform')
    nodes_info.append(node_info)
  print('nodes updated')
  return nodes_info

def update_links_info():
  links_info = []
  info = gns3.links_information(project_id)
  for link in info:
    link_info = {}
    link_info['link_id'] = link.get('link_id')
    link_info['node_id_1'] = link.get('nodes')[0].get('node_id')
    link_info['node_id_2'] = link.get('nodes')[1].get('node_id')
    links_info.append(link_info)
  print('link updated')
  return links_info

def create_node(project_id, name, node_type):
  final_para = None
  if node_type==0:
    final_para = '{"compute_id": "local", "name": "'+str(name)+'", "node_type": "vpcs"}'
    data = gns3.node_create(project_id, name, final_para)
  if node_type==1:
    f = open("property_7200.txt", "r")
    para = f.read()
    f.close()
    word = '"dynamips_id": '
    start_index = para.find(word)
    end_index = start_index + len(word)
    final_para = '{"compute_id": "local", "name": "'+'R'+str(name)+'", "node_type": "dynamips", "symbol": ":/symbols/router.svg", '+para[:end_index]+str(name)+para[end_index:]+'}'
    data = gns3.node_create(project_id, name, final_para)
    nodes_info = update_nodes_info()
    node_id = nodes_info[-1].get('node_id')
    f = open('startup_config.cfg', 'r')
    config_data = f.read()
    f.close()
    word = 'hostname R'
    start_index = config_data.find(word)
    end_index = start_index + len(word)
    config_data_final = config_data[:end_index]+str(name)+config_data[end_index:]
    f = open(dym_path+str(node_id)+'/configs/i'+str(name)+'_startup-config.cfg', 'w')
    f.write(config_data_final)
    # print(config_data_final)
    f.close()
  return data

def connect_console(PORT, name):
  tn = telnetlib.Telnet(HOST, PORT)

  tn.read_very_eager()
  tn.write(b"\r\n")
  tn.read_until((name+'#').encode())
  print('connected')
  return tn

def console_command(tn, name, cmd):
  tn.write(cmd.encode('ascii') + b"\r\n")
  data = tn.read_until((name+'#').encode()).decode('ascii')
  print(data)
  return data

def close_console(tn):
  tn.close()
  print('closed')

#TESTING
# tn = connect_console(5000, "R1")
# console_command(tn, "R1", "show int sum")
# console_command(tn, "R1", "show ip int bri")
# console_command(tn, "R1", "show int sum")
# console_command(tn, "R1", "show ip int bri")
# console_command(tn, "R1", "show int sum")
# console_command(tn, "R1", "show ip int bri")
# close_console(tn)