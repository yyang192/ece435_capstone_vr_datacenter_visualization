# ECE435 Capstone Project

## important information
Server receive the instruction command from client and returns the message containing the information requested or http respond code.  
The index of node or link in information list depends on the sequnce it was created.  
Command 14 will return the console information by the input command and will work only under command 13 is connected.   

## Server command instruction
first two characters of the command represent the index instruction  
00: version information  
01: nodes information  
02: links information  
03: create node  
04: delete node  
05: create link  
06: delete link  
07: start all nodes  
08: stop all nodes  
09: suspend all nodes  
10: start one node  
11: stop one node  
12: suspend one node  
13: connect console  
14: console command  
15: close console  
-1: close connection  (for test by client.py)  

## format  
<pre>
00: two digits 00  
01: two digits 01  
02: two digits 02  
03: two digits 03 + two digits of node type:   
    00->vpc; 01->router(c7200)  
    e.g. 0301: create a router  
04: two digits 04 + index of router  
    e.g. 0400: delete the first node in node information list  
05: two digits 05 + two digits first node + two digits second node + four digits first port + four digits second port  
    e.g. 050001f0/0f0/0: create the link between f0/0 on first node and f0/0 on second node  
06: two digits 06 + two digits link index  
    e.g. 0600: delelte the first link in link information list  
07: two digits 07  
08: two digits 08  
09: two digits 09  
10: two digits 10 + two digits node index  
    e.g. 1000: start first node in node information list  
11: two digits 11 + two digits node index  
12: two digits 12 + two digits node index  
13: two digits 13 + two digits node index  
    e.g. 1300 telnet connect to console of first node  
14: two digits 14 + two digits node index + command write to console  
    e.g. 1400show ip int bri: shows the interface information of first node  
15: two digits 15 
</pre>
