﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class serverPlayerControl : MonoBehaviour {

	public float speed;
	public GameObject server;
	public Transform server_c;
	
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

	private void update()
	{
		if (Input.GetKeyDown("tab"))
		{
			Instantiate(server, server_c.position, server_c.rotation);
		}
	}
	
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("door"))
        {
            other.gameObject.SetActive(false);
            SceneManager.LoadScene("ControlKEY");
        }
    }
    

}
