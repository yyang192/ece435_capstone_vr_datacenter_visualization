using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System;

public class Client : MonoBehaviour
{
    public int port;
    void Start()
    {
        //project create
        port = 8080;
        Debug.Log("UDPSTART");
        var client = new UdpClient();
        IPEndPoint ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
        client.Connect(ep);

        // send data
        byte[] data1 = Encoding.ASCII.GetBytes("unity 0");
        client.Send(data1, data1.Length);

        // receive data
        var receivedData = client.Receive(ref ep);

        // parse msg and set to player pref
        string msg = Encoding.UTF8.GetString(receivedData);
        var parts = msg.Split(' ');
        string first = parts[0];
        int second = 0;
        if (string.CompareOrdinal(first, "OK") == 0)
        {
            second = Convert.ToInt32(parts[1]);
            PlayerPrefs.SetInt("ID", second);
            Debug.Log("GLOBAL ID: " + second);
        }
        else {
            Debug.Log("ERROR CREATE PROJECT");
        }
        
        // player pref global access
        

        //string msg = Encoding.UTF8.GetString(receivedData);
        //Debug.Log("MSG: " + msg);



        /*
        TcpClient tcp = new TcpClient("127.0.0.1", 8080);
        Stream stream = tcp.GetStream();

        
        StreamWriter writer = new StreamWriter(stream);
        writer.WriteLine("Testing...");
        writer.Close();
        

       
        byte[] data1 = Encoding.ASCII.GetBytes("Testing...");

        stream.Write(data1, 0, data1.Length); 


        byte[] data = new byte[256];
       
        string responseData = string.Empty;
        
        int bytes = stream.Read(data, 0, data.Length); 
        responseData = Encoding.ASCII.GetString(data, 0, bytes); 
        Debug.Log(responseData);


        tcp.Close();
        */
    }

    void Update()
    {
    }

   
}